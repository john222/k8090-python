#!/usr/bin/python3

import sys
import time
import serial

class Velleman8090:
    def __init__(self):
        self.port = port
        self.baud_rate = 19200
        self.data_bits = 8
        self.parity = 'N'
        self.stop_bits = 1
        self.flow_control = 'N'

    def open_device(self):
        self.talk = serial.Serial(self.port, self.baud_rate, self.data_bits, self.parity, self.stop_bits)

    def firmware_version(self):
        data = packet(0x71, 0x00, 0x00, 0x00)
        self.talk.write(data)
        print(self.talk.read())

    def toogle_all_relays(self):
        data = packet(0x14, 0xff, 0x00, 0x00)
        self.talk.write(data)

    def turn_off_all_relays(self):
        data = packet(0x12, 0xff, 0x00, 0x00)
        self.talk.write(data)

    def turn_on_all_relays(self):
        data = packet(0x11, 0xff, 0x00, 0x00)
        self.talk.write(data)

    def turn_on_specific_relay(self, relay):
        data = packet(0x11, relay, 0x00, 0x00)
        self.talk.write(data)

    def turn_off_specific_relay(self, relay):
        data = packet(0x12, relay, 0x00, 0x00)
        self.talk.write(data)

    def close_device(self):
        self.talk.close()

def chksum(cmd,msk,p1,p2):
    return (((~(0x04 + cmd + msk + p1 + p2)) + 0x01) & 0xff)

def packet(cmd,msk,p1,p2):
    return bytearray([0x04, cmd, msk, p1, p2, chksum(cmd, msk, p1, p2), 0x0f])

port_list = ['/dev/ttyACM0', '/dev/ttyACM1', '/dev/ttyACM2']
relay_list = [0x1, 0x2, 0x4, 0x8, 0x10, 0x20, 0x40, 0x80]
number_of_relais = 8

for port in port_list:
    vm8090 = Velleman8090() # Initialize object
    vm8090.port = port # Assign port from port list
    vm8090.open_device() # Open connection to Velleman device
    vm8090.turn_off_all_relays() # Execute command
    for relay in relay_list:
        vm8090.turn_on_specific_relay(relay)
        time.sleep(0.2)
        vm8090.turn_off_specific_relay(relay)
    vm8090.close_device() # Close connection to Velleman device
